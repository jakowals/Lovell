// VeloMonitor defines an init method which, when invoked with the activePage,
// runs page-specific actions to do things like fetch job results and display
// plots
var VeloMonitor = (function(window, undefined) {
  'use strict';

  // Store all HTML up here because it looks messy inline with JS and this provides
  // a single place to edit all classes, elements, etc.
  var TEMPLATES = {}
  TEMPLATES.failureDiv = '<div class="alert alert-danger">{0}</div>';
  TEMPLATES.failureMsg = '<p>There was a problem retrieving plot '
    + '<code>{0}</code>. '
    + 'Please contact the administrator. Error message:</p>';
  TEMPLATES.dqTree = '<div class="dq-tree"><ul class="parent">{0}</ul></div>';
  TEMPLATES.dqTreeList = '<li><span class="toggle"><span class="label label-{2}">{1:.0f}</span> {0}</span><ul class="child">{3}</ul></li>';
  TEMPLATES.dqTreeItem = '<li>{0}: {1}</li>';

  // We need to check if a job result is an array or not, so polyfill the
  // isArray method on the Array object in case it isn't available
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
  if (!Array.isArray) {
    JobMonitor.log('Applying polyfill for Array.isArray')
    Array.isArray = function(arg) {
      return Object.prototype.toString.call(arg) === '[object Array]';
    };
  }

  // Return a string of random alphanumeric characters.
  //
  // http://stackoverflow.com/a/8084248/596068
  // Accepts:
  //   length: Length of string to return (default: 7)
  // Returns:
  //   String object of length `length`
  var randomString = function(length) {
    return (1 + Math.random()).toString(36).substring(7);
  };

  // Display msg inside container, styled as a red error box
  // Accepts:
  //   msg: HTML message to display inside container
  //   container: jQuery element to insert msg into
  // Returns:
  //   undefined
  var displayFailure = function(msg, container) {
    container.html(TEMPLATES.failureDiv.format(msg));
  };

  // Add a `Spinner` object to the `element`, using `JobMonitor.settings.spinnerDefaults` as options.
  // Accepts:
  //   element: jQuery element
  // Returns:
  //   Spinner object
  var appendSpinner = function($element) {
    var spinner = new Spinner(JobMonitor.settings.spinnerDefaults).spin($element.get(0));
    // We explicitly set the positioning of the spinner, as although the default
    // position is {top: '50%', left: '50%'}, this doesn't seem to work inside
    // elements hidden with 'display: none' unless we set it explicitly.
    spinner.el.style['top'] = '50%';
    spinner.el.style['left'] = '50%';
    return spinner;
  }

  // Keep track of running jobs, disabling all forms if there are any running
  // This is the simplest way of ensuring that there aren't multiple Promises
  // waiting to replace the current plots with new ones, i.e. ensuring there's
  // only one set of Promises at a time by not allowing the creation of new
  // ones via form submission
  var JobCounter = {
    _njobs: 0,
    increment: function() {
      this._njobs += 1;
      $('form :input').prop('disabled', true);
    },
    decrement: function() {
      this._njobs -= 1;
      if (this._njobs == 0) {
        $('form :input').prop('disabled', false);
      }
    }
  };

  // Load the plot(s) returned by the job task in to container.
  // Accepts:
  //   args: Object of keyword arguments to pass to the job
  //   container: jQuery element to load the resulting plot(s) in to, if any
  // Returns:
  //   Polling job task
  var loadTaskPlotInContainer = function(task, args, cacheKey, container) {
    var spinner = appendSpinner(container);

    // Use cached job result if available, else create a job
    // Promises used to unify resolution/failure logic
    var cacheResult = JobCache.getItem(cacheKey);
    var task;
    if (cacheResult !== null) {
      JobMonitor.log('Cache hit for key ' + cacheKey + '')
      task = $.Deferred();
      // Can always resolve as only successful jobs are cached
      task.resolve(cacheResult);
    } else {
      task = JobMonitor.createTask(task, args);
      JobCounter.increment();
    }

    task.done(function(job) {
      if (cacheResult === null) {
        JobCache.setItem(cacheKey, job);
      }
      var result = job['result'];
      // Add the JavaScript and HTML elements to the container
      // The Bokeh JS library will render the data in the JS to the HTML
      container.html(result[0] + result[1]);
    });

    task.fail(function(message) {
      var msg = TEMPLATES.failureMsg.format(args.plot_name) + message;
      displayFailure(msg, container);
    });

    task.always(function() {
      spinner.stop();
      // Don't need to touch the counter if there was a cache hit, i.e. no new
      // job created
      if (cacheResult === null) {
        JobCounter.decrement();
      }
    });

    return task;
  };

  // Each child page in the run view has its contents set by the run view
  // configuration dictionary
  // This sets what plots to retrieve from the run file
  // Each plot has a name, which is sent to the relevant run view job, along
  // with page-specific arguments like sensor number
  var runView = {
    init: function() {
      JobMonitor.log('runView.init');
      this.persistPlotDimensions();
      this.setupTabs();
      this.setupRunSelector();
      this.setupPlots();
      this.fixNavigationOverflow();
    },
    dq: {
      init: function() {
        this.setupDQTree();
      },
      setupDQTree: function() {
        var run = $('#run-number').data('run'),
            container = $('.run-view-pane'),
            cacheKey = 'dq-tree-' + run,
            cacheResult = JobCache.getItem(cacheKey),
            spinner = appendSpinner(container),
            task,
            callback = this.displayDQTree;
        if (cacheResult !== null) {
          callback(cacheResult);
        } else {
          task = JobMonitor.createTask('runview.dq_trends.get_dq_tree', {runnr: run});
          task.done(function(result) {
            JobCache.setItem(cacheKey, result);
            callback(result);
          });
          task.always(function() {
            spinner.stop();
          });
        }
      },
      displayDQTree: function(result) {
          var container = $('.run-view-pane'),
              tree = result['result'];

          if (tree === undefined || $.isEmptyObject(tree)) {
            displayFailure('Could not retrieve DQ tree for this run.', container);
            return;
          }

          // Don't want to display the run number or the timestamp
          delete tree.runnr;
          delete tree.timestamp;

          var labelClasses = ['success', 'warning', 'danger'];
          var formatLevel = function(tree) {
            var key, keyFmt, score, level, child, html = '';
            for (key in tree) {
              // We will display the values of these keys inline with the key
              // name, so don't need to display them separately
              if (key === 'score' || key === 'lvl') {
                continue;
              }
              // Try to format a pretty-ish title from the key name
              keyFmt = key.replace(/_/g, ' ');
              keyFmt = keyFmt[0].toUpperCase() + keyFmt.slice(1)

              child = tree[key];
              // An object means we'll be nesting, so recurse into the object
              if (typeof child === 'object') {
                score = child['score'];
                level = child['lvl'];
                html += TEMPLATES.dqTreeList.format(
                  keyFmt, score, labelClasses[level], formatLevel(child)
                );
              } else {
                // Otherwise we have an end node, so display the key and its value
                html += TEMPLATES.dqTreeItem.format(keyFmt, child);
              }
            }
            return html;
          };

          $('.run-view-pane').html(TEMPLATES.dqTree.format(formatLevel(tree)));

          // Allow each level to be toggled, hiding all its children
          $('.dq-tree').delegate('ul.parent > li .toggle', 'click', function(ev) {
            ev.preventDefault();
            $(this).parent().find('ul.child').first().slideToggle({duration: 100});
            $(this).toggleClass('closed');
          })
          // Hide all levels initially
          $('.dq-tree .toggle').trigger('click');
      }
    },
    // Load the plots in to the page
    // Accepts:
    //   args: Optional object of arguments to send to the job
    //         The name of the plot is sent automatically
    // Returns:
    //   undefined
    setupPlots: function(args) {
      var requiresSensorSelector = false;
      $('.plot').each(function(idx, el) {
        var $el = $(el),
            name = $el.data('name'),
            run = $('#run-number').data('run'),
            title = $el.data('title'),
            plotOpts = $el.data('plot-options') || {},
            sensorDependent = $el.data('sensor-dependent'),
            sensor = $el.data('sensor');

        args = args || {};
        args.plot_name = name;
        args.plot_opts = plotOpts;
        args.run = run;
        args.width = $el.width();
        args.height = $el.height();
        // If a plot is sensor dependent and a number is given, format the
        // job's plot name argument with the sensor number
        if (sensorDependent === 'True' && sensor !== '') {
          requiresSensorSelector = true;
          args.plot_name = name.format(sensor)
        }

        var cacheKey = args.plot_name + '_' + args.run;
        loadTaskPlotInContainer('tasks.runview_plot', args, cacheKey, $el);
      });
      // We don't do this inside the each loop else bindings will be duplicated
      if (requiresSensorSelector === true) {
        runView.setupSensorSelector();
      }
    },
    // Explicitly set the plot container's dimensions.
    //
    // As the .run-view-pane elements are hidden with display: none for the
    // tabbing feature, their dimensions are not correctly set by the time the 
    // plots are initialised.
    // Set their width and height inline here, before the tabbing is enabled.
    // ancestor.
    // Returns:
    //   undefined
    persistPlotDimensions: function() {
      $('.plot').each(function(idx, el) {
        var $el = $(el);
        $el.css({
          width: $el.width(),
          height: $el.height()
        });
      });
    },
    // Set up the tab switching mechanism.
    //
    // The showing of the panes is handled by Bootstrap, but we set a URL hash
    // when switching and read it on page load, allowing persistent tab state.
    // Returns:
    //   undefined
    setupTabs: function() {
      var $tabs = $('.run-view-tab'),
          $panes = $('.run-view-pane');

      // Enable the pane-hiding class
      $('.to-be-tab-content').attr('class', 'tab-content');

      // Update the URL hash on tab change
      // Append a '/' to the ID value so the page doesn't jump to the pane's
      // location
      $tabs.children('a').on('shown.bs.tab', function(e) {
        e.preventDefault();
        var tabID = $(e.target).attr('href').substr(1);
        window.location.hash = '/{0}'.format(tabID);
      });

      // Make the tab specified by the window hash active, or activate the
      // first tab if no hash is present
      // We need to strip the '/' off of the URL hash to match the anchor href
      var onHashChange = function() {
        var hash = window.location.hash.substr(2);
        if (hash === '') {
          $tabs.first().children('a').click();
        } else {
          $tabs.children('a[href="#{0}"]'.format(hash)).click();
        }
      };

      window.onhashchange = onHashChange;
      onHashChange();
    },
    // Set up the run selector form.
    //
    // The run selector form, shown in a modal window, shows both a select
    // of nearby runs, and an autocompleting textfield. It's just the textfield's
    // value that's used by the server, so when a run is picked using the select,
    // the textfield needs to be updated to the chosen value.
    // We also need to link the submit button outside the form element to the
    // form itself.
    // Returns:
    //   undefined
    setupRunSelector: function() {
      var $runSelector = $('.run-selector form'),
          $submitButton = $('.run-selector button[type=submit]'),
          $runField = $runSelector.find('input[type=text]');
      $runSelector.find('select').change(function() {
        $runField.val($(this).val());
      });
      $submitButton.click(function() {
        $runSelector.submit();
      });
    },
    // Set up bindings for sensor selector buttons
    //
    // When a VELO layout sensor number button is clicked, the sensor number
    // form is filled in appropriately and clicked
    // Returns:
    //   undefined
    setupSensorSelector: function() {
      // Make sure this function is imdempotent, as we don't want to
      // duplicate these bindings
      if (this.sensorSelectorReady === true) {
        return;
      }
      this.sensorSelectorReady = true;
      var $veloLayout = $('#velo-layout'),
          // Grab any form as they all submit to the same place
          $form = $('.sensor-selector'),
          $input = $form.find('input[type=text]');

      // Submit the sensor number form when a sensor number is clicked
      // TODO active sensor is not updated when new sensor is AJAX'd in
      $veloLayout.find('button:not([disabled])').on('click', function(e) {
        var $target = $(e.target);
        // Don't reload the page if we're already on that sensor
        if ($target.hasClass('active') === true) return;
        // Otherwise, fill in the sensor selector form and submit it
        $input.val($target.text());
        $veloLayout.modal('hide');
        $form.submit();
        return false;
      });

      // If the previous/next button is clicked, decrement/increment the sensor
      // number in the input Because the button's are type=submit, the click
      // event on button will fire first, then the submit event on the form,
      // using the updated sensor number
      $form.find('button[type=submit]').on('click', function() {
        // $form is all forms, so find the form elements of the button that was clicked
        var $target = $(this),
            $parentForm = $target.closest('.sensor-selector'),
            $siblingInput = $parentForm.find('input[type=text]'),
            diff = 0,
            sensorNumber = parseInt($siblingInput.val(), 10);

        // Increment/decrement the sensor number as requested, unless the
        // sensor number would become invalid or the text field is focused, in
        // which case the form should be submitted with the current value
        if ($target.hasClass('previous') === true && sensorNumber !== 0) {
          diff = -1;
        } else if ($target.hasClass('next') === true && sensorNumber !== 105) {
          diff = 1;
        }
        if ($siblingInput.is(':focus') === true) {
          diff = 0;
        }

        // We want to update *all* forms with the new value,
        // as all plots will be updated
        $input.val(sensorNumber + diff);
      });

      var updateSensorNumber = function(sensorNumber) {
        // We don't set attr data-sensor as it's data('sensor') that's used
        // elsewhere, and jQuery caches calls to data so won't see the updated
        // the DOM
        $('.plot').empty().data('sensor', sensorNumber);
        runView.setupPlots();
      };

      // Reload any sensor-dependent plots when the sensor selector is submitted
      // TODO corresponding r/phi sensor is not updated when new sensor is AJAX'd in
      $form.on('submit', function() {
        // Must be an integer so it can formatted in to strings in setupPlots
        var sensorNumber = parseInt($input.val(), 10);
        // Build the new URL, preserving the hash (i.e. the tab state)
        var url = $form.attr('action') + '/' + sensorNumber + window.location.hash;
        // Store the sensor number so it can be accessed on window popstate
        window.history.pushState({sensorNumber: sensorNumber}, null, url);
        updateSensorNumber(sensorNumber);
        return false;
      });

      // When the user hits the 'back' button, reload the sensor plots with
      // the 'old' sensor number
      window.onpopstate = function(e) {
        if (e.state !== null && e.state.sensorNumber !== undefined) {
          updateSensorNumber(e.state.sensorNumber);
        }
      };
    },
    // Adjust tabbed navigation to accommodate long tab labels and singular
    // plots
    //
    // The navigation doesn't display correctly with long labels, i.e. that
    // have multiple lines. This fixes that by making all tabs have the same
    // height as the tallest one, and also hides the navigation if there's only
    // tab.
    // Returns:
    //   undefined
    fixNavigationOverflow: function() {
      var navItems = $('.nav-tabs.nav-justified').find('li');
      if (navItems.length == 1) {
          navItems.hide();
      }
      // Find the tallest list item (the <li> element wraps the <a>)
      var heights = navItems.map(function() {
          return $(this).height();
      }).get();
      var maxHeight = Math.max.apply(Math, heights);
      // Force every <a> to have the same height as the tallest <li>
      navItems.find('a').innerHeight(maxHeight);
    }
  };

  var veloView = {
    trends: {
      init: function() {
        $('form.trending').submit(function(e) {
          var $this = $(this),
              selector = $this.find('select'),
              startInput = $this.find('input[name=start-run]'),
              endInput = $this.find('input[name=end-run]'),
              trendable = selector.val(),
              startRun = parseInt(startInput.val(), 10),
              endRun = parseInt(endInput.val(), 10),
              runs = [];

          if (trendable === null) {
            return false;
          }
          // TODO find more sensible/rationalised default values
          if (Number.isNaN(startRun)) {
            startRun = 0;
          }
          if (Number.isNaN(endRun)) {
            endRun = 999999;
          }
          runs = [startRun, endRun];


          var $el = $('.trend-plot'),
              plotOpts = {
                showPoints: true,
                interpolation: 'linear',
                // We don't want the run numbers to be formatted with exponents
                xFormatExponent: false
              },
              args = {
                name: trendable,
                run_range: runs,
                plot_opts: plotOpts,
                width: $el.width(),
                height: $el.height()
              },
              cacheKey = args.name + '_' + startRun + '_' + endRun;

          loadTaskPlotInContainer(
            'tasks.trending_plot',
            args, cacheKey, $el
          );

          e.preventDefault();
        });
      }
    }
  };

  var specialAnalyses = {
    ivScans: {
      init: function() {
        $('.plot').each(function(idx, el) {
          var $el = $(el),
              scan = $el.data('scan'),
              reference = $el.data('reference'),
              sensor = $el.data('sensor'),
              corrected = $el.data('temperature-corrected'),
              args = {};

          args.scan = scan;
          // Don't show the reference if it's the same as the nominal
          if (reference != scan) {
            args.reference = reference;
          } else {
            args.reference = null;
          }
          args.sensor = sensor;
          args.width = $el.width();
          args.height = $el.height();

          specialAnalyses.setupModuleSelector();

          var cacheKey = args.scan + '_' + args.reference + '_' + args.sensor;
          if (corrected) {
            cacheKey += '_corrected'
            loadTaskPlotInContainer('tasks.iv_scan_corrected_plot', args, cacheKey, $el);
          } else {
            loadTaskPlotInContainer('tasks.iv_scan_plot', args, cacheKey, $el);
          }
        });
      }
    },
    // Set up bindings for module selector buttons
    //
    // Returns:
    //   undefined
    setupModuleSelector: function() {
      // Make sure this function is imdempotent, as we don't want to
      // duplicate these bindings
      if (this.sensorSelectorReady === true) {
        return;
      }
      this.sensorSelectorReady = true;
      var $form = $('.module-selector'),
          $input = $form.find('input[type=text]');

      // If the previous/next button is clicked, decrement/increment the module
      // number in the input
      // Because the button's are type=submit, the click event on button will
      // fire first, then the submit event on the form, using the updated
      // module number
      $form.find('button[type=submit]').on('click', function(e) {
        // $form is all forms, so find the form elements of the button that was clicked
        var $target = $(this),
            $parentForm = $target.closest('.module-selector'),
            $siblingInput = $parentForm.find('input[type=text]'),
            diff = 0,
            moduleNumber = parseInt($siblingInput.val(), 10);

        // Increment/decrement the module number as requested, unless the
        // module number would become invalid or the text field is focused, in
        // which case the form should be submitted with the current value
        if ($target.hasClass('previous') === true && moduleNumber !== 0) {
          diff = -1;
        } else if ($target.hasClass('next') === true && moduleNumber !== 41) {
          diff = 1;
        }
        if ($siblingInput.is(':focus') === true) {
          diff = 0;
        }

        // We want to update *all* forms with the new value,
        // as all plots will be updated
        $input.val(moduleNumber + diff);
      });
    },
  };

  var settings = {
    init: function() {
      $('#clear-plot-cache').on('submit', function(ev) {
        JobCache.clear();
        var $submit = $(this).find('button[type=submit]').last();
        var originalText = $submit.text();
        // Show the user that something has happened
        $submit.removeClass('btn-warning')
               .addClass('btn-success')
               .text('Plot cache reset!')
               .prop('disabled', true);
        // Reset the button to its original state after five seconds
        setTimeout(function() {
          $submit.removeClass('btn-success')
                 .addClass('btn-warning')
                 .text(originalText)
                 .prop('disabled', false);
        }, 5000);
        ev.preventDefault();
      });
    }
  };

  // Page-specific modules
  var pages = {
    runView: runView,
    veloView: veloView,
    specialAnalyses: specialAnalyses,
    settings: settings
  };

  // Initialise the app
  var init = function(pageModule) {
    JobMonitor.init(pageModule, pages);
  };

  return {
    init: init
  };
})(window);

$(function() {
  // Poll running jobs every 500 milliseconds
  JobMonitor.settings.pollRate = 500;
  VeloMonitor.init(activePage);
});
