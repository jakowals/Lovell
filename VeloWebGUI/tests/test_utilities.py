from StringIO import StringIO
import time
import unittest

from velo_monitor.utilities import memoize


class TestPages(unittest.TestCase):
    def test_memoize(self):
        """Memoize should cache function calls by argument values."""
        msg = 'f called with {0} {1}'

        @memoize(refresh_rate=-1)
        def f(a, b, out):
            out.write(msg.format(a, b))
            return a*b

        a = 2
        b = 3
        out = StringIO()
        msg_ab = msg.format(a, b)

        # The first call should return the correct value, and should write a
        # message to the StringIO instance
        ret1 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), msg_ab)
        self.assertEqual(ret1, a*b)

        # The second call should also return the correct value, but shouldn't
        # write additional content to the StringIO instance, and the memoize
        # cache should just return the return value
        # If f was really being executed again, the value of the StringIO
        # instance will have the second message appended to it
        ret2 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), msg_ab)
        self.assertEqual(ret2, a*b)

        # A call with different arguments should write a different message, and
        # should return a different (but still correct) value
        ret3 = f(2*a, b, out)
        self.assertEqual(out.getvalue().strip(), msg_ab + msg.format(2*a, b))
        self.assertEqual(ret3, 2*a*b)

    def test_memoize_refresh(self):
        """Memoize should expire a cache after a timeout"""
        msg = 'f called with {0} {1}'

        # Expire the cache after 1 second
        refresh_rate = 1

        @memoize(refresh_rate=refresh_rate)
        def f(a, b, out):
            out.write(msg.format(a, b))
            return a*b

        a = 2
        b = 3
        out = StringIO()
        msg_ab = msg.format(a, b)

        # We proceed initially as in test_memoize
        ret1 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), msg_ab)
        self.assertEqual(ret1, a*b)

        # Then, sleep for a bit less than the cache expire time
        time.sleep(refresh_rate - 1)
        # This call should happen very quickly, before the cache expires
        ret2 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), msg_ab)
        self.assertEqual(ret2, a*b)

        # Then, sleep so that we're definitely over the expire time, now having
        # slept for (refresh_rate - 1) + 2 = refresh_rate + 1
        time.sleep(2)

        # The cache should have expired, so we'll have additional content from
        # the write to the StringIO object
        ret3 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), 2*msg_ab)
        self.assertEqual(ret3, a*b)
        # Can call the function again before the cache expires
        ret4 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), 2*msg_ab)
        self.assertEqual(ret4, a*b)

        # Cycle once more, to test that the refresh rate is cyclic
        time.sleep(2)
        ret5 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), 3*msg_ab)
        self.assertEqual(ret5, a*b)
        ret6 = f(a, b, out)
        self.assertEqual(out.getvalue().strip(), 3*msg_ab)
        self.assertEqual(ret6, a*b)
