# -*- coding: utf-8 -*-
"""Interface tasks between analysis framework methods to Bokeh figures."""
from array import array
import logging

from bokeh.embed import components
import ROOT
from rq.utils import import_attribute

from velo_monitor import config, plotting
from veloview.runview import plots, response_formatters
import veloview.runview.utils as runview_utils
from veloview.special import iv_scans


def execute_job(name, args):
    f = import_attribute(name)
    return f(**args)


def components_from_figure(fig):
    """Return (script, HTML) components for the Bokeh figure."""
    script, html = components(fig)
    return [script, html]


def runview_plot(plot_name, plot_opts, run, width, height):
    """Return Bokeh components for plot from the given run.

    Keyword arguments:
    plot_name -- Passed to get_run_plot_with_reference
    plot_opts -- Passed to the plot renderer to set style attributes
    run -- Passed to get_run_plot_with_reference
    width -- Plot width in pixels
    height -- Plot height in pixels
    """
    # The requested plot may be composed of many 'plottables'
    # We'll send one request to the framework per plottable, omitting the
    # reference plot as things will look two messy with 4+ sub-plots
    # Use str so we don't pass a unicode string (TFile.Get doesn't
    # understand them)
    names = map(str, plot_name.split(config.PLOTTABLES_SEPARATOR))

    normalise = plot_opts.get('normalise', False)

    if len(names) > 1:
        job = 'veloview.runview.plots.get_run_plot'
        data = [
            execute_job(job, dict(name=name, run=run, normalise=normalise))
            for name in names
        ]
    else:
        job = 'veloview.runview.plots.get_run_plot_with_reference'
        data = execute_job(
            job, dict(name=names[0], run=run, normalise=normalise)
        )

        # Add the run numbers as legend labels
        nominal_run = run
        reference_run = runview_utils.reference_run(plot_name, nominal_run)
        runs = [nominal_run, reference_run]
        for run, d in zip(runs, data):
            if d is not None:
                d['data']['label'] = 'Run {0}'.format(run)

    # Some plots may be missing
    # TODO: show an error if one or more subplots are missing
    data = [d for d in data if d is not None]
    for d in data:
        if not d['success']:
            # TODO: Return a proper error
            return False

    fig = plotting.job_to_figure(data, plot_opts, width, height)
    return components_from_figure(fig)


def iv_scan_plot(scan, reference, sensor, width, height):
    """Return Bokeh components for the given IV scan."""
    nom, ref, nomtemp, reftemp = iv_scans.iv_scan(
        scan, ref_file=reference, sensor_id=sensor
    )

    if nom is not None:
        nom['data']['label'] = 'Nominal ({0:.1f} °C)'.format(nomtemp)
    if ref is not None:
        ref['data']['label'] = 'Reference ({0:.1f} °C)'.format(reftemp)

    data = [d for d in [nom, ref] if d is not None]

    fig = plotting.job_to_figure(data, dict(), width, height)

    return components_from_figure(fig)


def iv_scan_corrected_plot(scan, reference, sensor, width, height):
    """Return Bokeh components for the given IT scan."""
    nom, ref = iv_scans.iv_scan_corrected(
        scan, ref_file=reference, sensor_id=sensor
    )

    if nom is not None:
        nom['data']['label'] = 'Nominal'
    if ref is not None:
        ref['data']['label'] = 'Reference'

    data = [d for d in [nom, ref] if d is not None]

    fig = plotting.job_to_figure(data, dict(), width, height)

    return components_from_figure(fig)


def trending_plot(name, run_range, plot_opts, width, height):
    payload = plots.get_trending_plot(name, run_range)
    data = payload['data']['data']
    xtitle, ytitle = data['axis_titles']
    # Remove runs with no corresponding value for the trended quantity
    values = [(r, v) for (r, v) in data['values'] if v is not None]
    if values:
        xs, ys = zip(*values)
    else:
        logging.warning('Trends for {0} were all None in run range {1}'.format(
            name, run_range
        ))
        xs, ys = [0], [0]
    assert len(xs) == len(ys)

    # We want to display the data as a graph of (x, y) points, so it's simplest
    # to reuse all the from-ROOT-objects logic by converting the data first to
    # a TGraph, and then passing the dictionary representation of that to the
    # plotter
    plot = ROOT.TGraph(len(xs), array('f', xs), array('f', ys))
    plot.GetXaxis().SetTitle(xtitle)
    plot.GetYaxis().SetTitle(ytitle)
    plot_as_dict = response_formatters.tobject_formatter(plot)

    fig = plotting.job_to_figure([plot_as_dict], dict(), width, height)

    return components_from_figure(fig)
