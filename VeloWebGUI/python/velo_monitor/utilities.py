import functools
import time


class memoize(object):
    """Temporarily cache function calls that have identical arguments.

    The refresh_rate attribute is the number of seconds after which the cache
    should be reset to empty. A negative value implies that the cache should
    never expire. The refresh rate is an offset from the time memoize is
    instantiated, i.e. when the decorated function becomes decorated. The cache
    is cleared for all values of the arguments of the decorated function.

    Example usage:

        >>> @memoize()
        ... def f(a, b):
              print 'f called', a, b
              return a*b
        >>> f(1, 2)
        f called 1 2
        2
        >>> f(1, 2)
        2
        >>> f(2, 3)
        f called 2 3
        6

    Implementation based on https://wiki.python.org/moin/PythonDecoratorLibrary
    with the addition of the expiry.
    """
    def __init__(self, refresh_rate=-1):
        """Instantiate a memoize decorator object."""
        self.cache = {}

        if refresh_rate > 0:
            curr_time = time.time()
            self.refresh_rate = refresh_rate
            self.expires_at = curr_time + self.refresh_rate
        else:
            self.refresh_rate = None
            self.expires_at = None

    def __call__(self, func):
        """Called by the decorator @-syntax.

        The class can be used in two ways, firstly using the @-syntax

            @memoize(refresh_rate=5)
            def some_func(...):
                # ...

        and secondly with explicit function reassignment:

            def some_func(...):
                # ...

            some_func = memoize(refresh_rate=5)(some_func)

        The second call in the second example, with some_func as an argument,
        is to this __call__ method. The @-syntax executes it implicitly.
        """
        # So that we can still reference this memoize instance below
        this = self

        @functools.wraps(func)
        def memoizer(*args, **kwargs):
            # Clear the cache if the expiry time is defined and in the past
            curr_time = time.time()
            if this.expires_at is not None and this.expires_at < curr_time:
                this.cache.clear()
                this.expires_at = curr_time + this.refresh_rate

            # Can't store args or kwargs in cache dict if they're not hashable
            # 'Hashing' them as strings is not perfect, but probably good
            # enough for us, see http://stackoverflow.com/a/6408175/596068
            key = (str(args), str(kwargs))
            if key not in this.cache:
                this.cache[key] = func(*args, **kwargs)
            return this.cache[key]

        return memoizer
