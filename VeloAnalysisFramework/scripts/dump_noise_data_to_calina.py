#'/calib/velo/dqm/VeloView/VetraOutput'

import sys
import os
from veloview.runview import utils
from veloview.runview import plots
from veloview.config import run_view
import logging
import argparse
from collections import namedtuple
import urllib2
import json
import csv
import time
import httplib
import re

logging.basicConfig(format='%(asctime)s [%(levelname)s]= %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def rundbapi(runid):
	request = 'https://lbrundb.cern.ch/api/run/{}/'.format(runid)
	response = urllib2.urlopen(request)
	if response.getcode() != 200:
		raise ValueError(
			'RunDB request invalid {}, check run number.'.format(response.getcode()))
	else:
		return json.loads(response.read())

class RunViewDumper(object):
	sensor_numbers = list(range(42)) + \
		list(range(64, 106)) + list(range(128, 132))

	def __init__(self, root_parameter_format, dump_name_format, dump_name):
		self.root_parameter_format = root_parameter_format
		self.dump_name_format = dump_name_format
		self.data_name = dump_name

	def read_data(self, parameter_name, run_number):
		try:
			return plots.get_run_plot(parameter_name, run_number, normalise=False, notify_box=None)
		except KeyError:
			logger.error("no data in {} run={}. Skipping...".format(
				parameter_name, run_number))
			return None

	def create_row_data(self, data, sensor, rundata):
		boardname = 'VeloTELL1Board{}'.format(sensor)
		dataname = self.data_name
		data = data['data']['data']['values']
		data = ['{0:.2f}'.format(value) for value in data]
		start, end = rundata['starttime'], rundata['endtime']
		run_number = rundata['runid']
		row_data = [dataname, run_number, boardname, start, end] + data
		return row_data

	def dump_run(self, dumpfilename, rundata):
		try:
			with open(dumpfilename, 'wa') as f:
				writer = csv.writer(f, delimiter=' ')
				for sensor in self.sensor_numbers:
					parameter_name = self.root_parameter_format.format(sensor)
					data = self.read_data(parameter_name, rundata['runid'])
					if data == None:
						break
					row_data = self.create_row_data(data, sensor, rundata)
					writer.writerow(row_data)
		except:
			if os.path.exists(dumpfilename):
				os.remove(dumpfilename)
			logger.error("During writing some error occured, deleting file {}".format(dumpfilename))
			raise

	def dump_all(self, dump_path, runlist):
		for runnum in runlist:
			logger.debug('Dumping {} run {}'.format(self.data_name, runnum))
			unsuccessful = True
			while unsuccessful:
				try:
					rundata = rundbapi(runnum)
					dumpfilename = self.dump_name_format.format(**rundata)
					dumpfilename = os.path.join(dump_path, dumpfilename)
					self.dump_run(dumpfilename, rundata)
				except IOError:
					logger.error(
						'Run {} not found, skipping... '.format(runnum))
					unsuccessful = False
				except httplib.BadStatusLine:
					logger.error(
						'DBApi returned uknown status code, waiting 1 second, to repeat.')
					time.sleep(1)
					unsuccessful = True
				else:
					unsuccessful = False


class DumpManager(object):
	def __init__(self, dump_destination, run_list):
		self.dump_destination = dump_destination
		self.run_list = run_list
		self.dump_types = {
			'noise':
				dict(
					root_format='Vetra/NoiseMon/DecodedADC/TELL1_{0:03d}/RMSNoise_vs_ChipChannel',
					dump_name_format='{runid}_{starttime}_{endtime}_noise_run_dump.csv',
					dump_type='noise'
				),

			'pedestals':
				dict(
					root_format='Vetra/VeloPedestalSubtractorMoni/TELL1_{0:03d}/Pedestal_Bank',
					dump_name_format='{}_pedestal_run_dump.csv',
					dump_type='pedestal'
				),

			'pedestals_subtracted':
				dict(
					root_format='Vetra/VeloPedestalSubtractorMoni/TELL1_{0:03d}/Ped_Sub_ADCs_Profile',
					dump_name_format='{}_pedestal_sub_run_dump.csv',
					dump_type='pedestal_subtracted'
				)
		}

	def generic_dumper(self, root_format, dump_name_format, dump_type):
		dumper = RunViewDumper(root_format, dump_name_format, dump_type)
		dumper.dump_all(self.dump_destination, self.run_list)

	def work(self, dump_type):
		self.generic_dumper(**self.dump_types[dump_type])


def get_runs_from_plus_file():
	with open('/calib/velo/dqm/VeloView/VetraOutput/RunList_Merged_NZS_Clusters_Brunel.txt', 'r') as f:
		run_list = f.read()
	run_list = run_list.split('\n')
	run_list = filter(lambda x: len(x) != 0 and not x.isspace(), run_list)
	run_list = [int(run) for run in run_list]
	return run_list

def filter_filepath(regex_format, filepaths):
	regex = re.compile(regex_format)
	condition = lambda x: regex.match(x)
	selected_files = filter(condition, filepaths)
	return list(selected_files)

def get_runs_from_dir(regex_format, dirpath):
	file_list = os.listdir(dirpath)
	selected_files = filter_filepath(regex_format, file_list)
	return selected_files

def get_dumped_runs_from_folder(folder):
	datetime_regex_format = '\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}'
	filename_regex_format = r'\d+_{}_{}_\S+_run_dump.csv'.format(datetime_regex_format, datetime_regex_format)
	filenames = get_runs_from_dir(filename_regex_format, folder)
	runs = map(lambda x: int(x.split("_")[0]), filenames)
	return runs


def main(args):
	run_list = get_runs_from_plus_file()
	folder = '{}/'.format(dump_type) if args.directory is not None else ''
	dump_path = os.path.join(args.destination, folder)
	if args.run_start is None and args.run_end is None and args.missing is None:
		selected_runs = run_list
	elif args.missing is None:
		min_list = min(run_list)
		max_list = max(run_list)
		range_start = min_list if args.run_start is None else max((args.run_start, min_list))
		range_end = max_list if args.run_end is None else min((args.run_end, max_list))
		runs = range(range_start, range_end)
		selected_runs = sorted(list(set(runs).intersection(run_list)))
	else:
		dumped_runs = get_dumped_runs_from_folder(dump_path)
		missing = list(set(run_list)-set(dumped_runs))
		selected_runs = missing
		logger.info('Dumping missing {} files.'.format(len(selected_runs)))
	print selected_runs
	RunViewDumper.sensor_numbers = list(range(42)) + list(range(64, 106))
	dumper = DumpManager(dump_path, selected_runs)
	dumper.work(args.dump_type)
	return 0


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description='Connects to rundb via rundb api, and '
		'dumps desired run data the runs are cross-referenced with the run file on plus.'
		'Only run range within the runs in the file will be dumped.'
		)
	parser.add_argument('dump_type', type=str,
						help='dump type, [noise, pedestals, pedestals_subtracted]')
	parser.add_argument('destination', type=str,
						help='path to destination for created csv files')
	parser.add_argument('--run_start', type=int, default=None,
						help='Start dump with this run')
	parser.add_argument('--run_end', type=int, default=None,
						help='end dump on this run')
	parser.add_argument('-d', dest='directory', type=str, default=None, 
						help='in specified destination, dump into this directory')
	parser.add_argument('--missing', dest='missing', action='store_true', default=False,
						help='when used downloads only files that are contained in RunList_Merged_NZS_Clusters_Brunel'
							 'but are not detected in destination file')

	args = parser.parse_args()
	sys.exit(main(args))
