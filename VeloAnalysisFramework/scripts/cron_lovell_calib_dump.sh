#!/bin/bash --login

# for debugging
# set -o xtrace

# setup
declare script=$(readlink -e $BASH_SOURCE)
declare srcdir="${script%/*}"

# save job arguments
declare jobargs="$@"
shift $#

# initialise environment; LbLogin et al.
source /cvmfs/lhcb.cern.ch/group_login.sh &> /dev/null
echo ${LBSCRIPTS_HOME}

# set resource limits
ulimit -Sm 2500000
ulimit -Sv 2500000
echo "Running on:" $HOSTNAME

# run job
export VELOSQLITEDBPATH=/group/velo/config/scratch_recipe/velocond/db
SCRIPT_PATH='/calib/velo/dqm/lhcb-prerelease/Today/LOVELL/LOVELL_HEAD/VeloAnalysisFramework/scripts'
VELOCOND_DATA='/calib/velo/dqm/tell1_velocond_data'

lb-run LHCb/latest python $SCRIPT_PATH/veloconddb_dumper.py $VELOCOND_DATA/Tell1Dump
lb-run LHCb/latest python $SCRIPT_PATH/tell1_calib_params_root.py  $VELOCOND_DATA/Tell1Dump  $VELOCOND_DATA/Tell1CalibRoot --root --run_list
lb-run LHCb/latest python $SCRIPT_PATH/tell1_calib_params_root.py  $VELOCOND_DATA/Tell1Dump  $VELOCOND_DATA/Tell1CalibCsv --csv --plain_dump --run_list




