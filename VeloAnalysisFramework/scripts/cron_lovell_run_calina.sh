#!/bin/bash --login

# for debugging
# set -o xtrace

# setup
declare script=$(readlink -e $BASH_SOURCE)
declare srcdir="${script%/*}"

# save job arguments
declare jobargs="$@"
shift $#

# initialise environment; LbLogin et al.
source /cvmfs/lhcb.cern.ch/group_login.sh &> /dev/null
echo ${LBSCRIPTS_HOME}

# set resource limits
ulimit -Sm 2500000
ulimit -Sv 2500000
echo "Running on:" $HOSTNAME

# run job

analyzer_path='/calib/velo/dqm/calina/calina/analyzer.py'
run_list_path='/calib/velo/dqm/tell1_velocond_data/Tell1CalibCsv/RunList.txt'
database_path='/calib/velo/dqm/calina/calina/database.db'
/calib/velo/dqm/calina/calina/env/bin/python3 $analyzer_path $run_list_path --run-list --do_calibration -d $database_path
/calib/velo/dqm/calina/calina/env/bin/python3 $analyzer_path $run_list_path --run-list --do_sensor -d $database_path






