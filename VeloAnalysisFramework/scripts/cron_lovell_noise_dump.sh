#!/bin/bash --login

# for debugging
# set -o xtrace

# setup
declare script=$(readlink -e $BASH_SOURCE)
declare srcdir="${script%/*}"

# save job arguments
declare jobargs="$@"
shift $#

# initialise environment; LbLogin et al.
source /cvmfs/lhcb.cern.ch/group_login.sh &> /dev/null
echo ${LBSCRIPTS_HOME}

# set resource limits
ulimit -Sm 2500000
ulimit -Sv 2500000
echo "Running on:" $HOSTNAME

# run job
export VELOSQLITEDBPATH=/group/velo/config/scratch_recipe/velocond/db
SCRIPT_PATH='/calib/velo/dqm/lhcb-prerelease/Today/LOVELL/LOVELL_HEAD/VeloAnalysisFramework/scripts'
VELOCOND_DATA='/calib/velo/dqm/tell1_velocond_data'
LOVELL_PATH='/calib/velo/dqm/lhcb-prerelease/Today/LOVELL/LOVELL_HEAD'

$LOVELL_PATH/build.$CMTCONFIG/run python $SCRIPT_PATH/dump_noise_data_to_calina.py noise $VELOCOND_DATA/NoiseRunCsv --missing



