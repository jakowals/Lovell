"""
This file defines classes that represent nodes in the data quality analysis
tree.
"""
from abc import ABCMeta, abstractmethod
import os

import ROOT

from ..config import Config
from ..utils.rootutils import detect_zombies
from ..utils.rundb import RunDB
from .exceptions import DescriptionDictionaryKeyException, \
        ComparisonFunctionNotFoundInEvalDictException, ComparisonFunctionNotCallableException, \
        HistogramNotFoundException
from .interface import ComparisonFunction
from .io import DQDB
from .score_manipulation import Score, ERROR_LEVELS

class Combiner(object):
    """
    The abstract base class for any node in the DQ analysis tree.

    An instance of this class represents a node in the DQ analysis tree. A node
    can be either a branch (BranchCombiner), which has one or more child nodes
    and information on how to combine the error levels of its child nodes into a
    single result, or a leaf node (LeafCombiner), which contains information on
    how to generate an error level for a specific histogram.
    """
    __metaclass__ = ABCMeta # Abstract base class

    def __init__(self, name, desc_dict, data_file_path, ref_file_path):
        """
        Super constructor for abstract Combiner class. Only to be called from
        subclass constructors.

        Parameters:
            name          : name of a combiner (must be consistent with desc and eval dicts)
            desc_dict     : a combiner description dictionary (which describes the tree structure)
            data_file_path: a path to a root file with data to analyse
            ref_file_path : a path to a root file that serves as reference
        """
        self.__name = name
        self._error_lvls = {lvl: 0 for lvl in ERROR_LEVELS}
        self._data_file_path = data_file_path
        self._ref_file_path = ref_file_path
        self._results = None

        self._desc_dict = self.__clipDescDict(desc_dict)
        self.__weight = self._desc_dict["weight"]

    def string_rep(self):
        """Creates a string representation of a combiner for logging purposes"""

        log_string = ["*********************************************************"]
        log_string.append("Combiner's name: {}".format(self.get_name()))
        log_string.append("Combiner's description: {}".format(self._desc_dict))
        log_string.append("Combiner's evaluation results: {}".format(self.get_results()))

        return log_string

    def __clipDescDict(self, desc_dict):
        """Clips the description dictionary so that all combiners don't need to store the entire dict in their memory"""
        try:
            return desc_dict[self.get_name()]
        except KeyError:
            raise DescriptionDictionaryKeyException(self.get_name(), desc_dict.keys())

    def get_name(self):
        """Get the name of this Combiner node"""
        return self.__name

    def get_weight(self):
        """Get the weight of this Combiner node (relative to that of its siblings)"""
        return self.__weight

    @classmethod
    def create(cls, name, desc_dict, eval_dict, data_file_path, ref_file_path):
        """
        Create a Combiner instance, either a BranchCombiner (if it has
        children) or a leaf combiner (if it contains a histogram comparison).
        """
        desc = desc_dict.get(name, None)
        if not desc:
            return None

        if "children" in desc.keys():
            return BranchCombiner(name, desc_dict, eval_dict, data_file_path, ref_file_path)
        else:
            return LeafCombiner(name, desc_dict, eval_dict, data_file_path, ref_file_path)

    @abstractmethod
    def evaluate(self):
        """
        Evaluate this combiner. Abstract method, to be implemented by
        subclasses.
        """
        pass

    @abstractmethod
    def is_relevant(self):
        """
        Whether this Combiner instance is relevant to evaluate.
        """
        pass

    def get_results(self):
        self.evaluate()
        return self._results

    def get_level(self):
        return self.get_results()["lvl"]

    def get_score(self):
        if "score" not in self.get_results(): return Score.MIN
        return self.get_results()["score"]

    def get_error_lvls(self):
        return self._error_lvls

    def get_warnings(self):
        """
        Returns number of warnings for this Combiner (errors are also counted
        as warnings).
        """
        return self._error_lvls[ERROR_LEVELS.WARNING] + self._error_lvls[ERROR_LEVELS.ERROR]

    def get_errors(self):
        """
        Returns number of errors for this Combiner (fatals are also counted as
        errors).
        """
        return self._error_lvls[ERROR_LEVELS.ERROR] + self._error_lvls[ERROR_LEVELS.FATAL]

    def get_fatals(self):
        """
        Returns number of fatals for this Combiner.
        """
        return self._error_lvls[ERROR_LEVELS.FATAL]

    def _get_writable_results(self):
        """
        Get a dictionary containing the score and level values. First evaluates
        this Combiner if necessary.
        """
        return {k: (v() if hasattr(v, '__call__') else v) for k,v in self.get_results().iteritems()}

class BranchCombiner(Combiner):
    def __init__(self, name, desc_dict, eval_dict, data_file_path, ref_file_path):
        """
        Parameters:
            desc_dict     : a combiner description dictionary (which describes the tree structure)
            eval_dict     : an evaluation dictionary that holds information about analysis method for each histogram
            data_file_path: a path to a root file with data to analyse
            ref_file_path : a path to a root file that serves as reference
            name          : name of a combiner (must be consistent with desc and eval dicts)
        """

        super(BranchCombiner, self).__init__(name, desc_dict, data_file_path, ref_file_path)
        self.__thresholds = self._desc_dict["thresholds"]
        self.__create_children(desc_dict, eval_dict)

    def __str__(self):
        """String representation of this BranchCombiner."""
        log_string = self.string_rep()

        children_names = [child.get_name() for child in self.get_children()]
        log_string.append("Combiner's error levels: {}".format(self.get_error_lvls()))
        log_string.append("Children:")
        for child in self.get_children():
            childDesc = "\tBranch: {} (weight {})\tScore: {} \tLevel: {} \tError levels: {}".format(child.get_name(), child.get_weight(), child.get_score(), child.get_level(), child.get_error_lvls())
            if isinstance(child, LeafCombiner):
                childDesc += "\tPath: {}".format(child.get_path())
            log_string.append(childDesc)

        for child in self.get_children():
            if isinstance(child, BranchCombiner):
                log_string.append(str(child))
        log_string.append("*********************************************************")

        return "\n".join(log_string)

    def __create_children(self, desc_dict, eval_dict):
        """Creates children for this BranchCombiner."""
        my_desc = desc_dict[self.get_name()]
        if not my_desc or not "children" in my_desc.keys():
            return []

        self.__children = []
        for combiner_name in my_desc["children"]:
            child = Combiner.create(combiner_name, desc_dict, eval_dict, self._data_file_path, self._ref_file_path)
            if child and child.is_relevant():
                self.__children.append(child)

        self.__sum_child_weights = sum([child._desc_dict["weight"] for child in self.get_children()])

    def get_children(self):
        return self.__children

    def __get_thresholds(self, lvl):
        return self.__thresholds.get(lvl, {})

    def __calc_lvl(self):
        """
        Calculates level depending on the description dictionary/expected
        state.
        """
        resulting = []
        for lvl in ERROR_LEVELS:
            resulting.append(self.__map_nr_to_lvl(self.get_error_lvls()[lvl], self.__get_thresholds(lvl)))
        return max(resulting)

    @staticmethod
    def __map_nr_to_lvl(number, thresholds):
        """
        Maps number of warnings/errors to a specific error state.
        @returns a value of the enum ERROR_LEVELS.
        """
        for lvl in reversed(ERROR_LEVELS):
            if lvl in thresholds and number >= thresholds[lvl]:
                return lvl
        return ERROR_LEVELS.OK

    def __calc_results(self):
        """
        Calculates combiner's score and level.
        """
        score = Score(Score.MIN)

        for child in self.get_children():
            score += child.get_score() * child.get_weight() / self.__sum_child_weights
            child_error_lvls = child.get_error_lvls()
            for k in child_error_lvls:
                self._error_lvls[k] += child_error_lvls[k]

        lvl = self.__calc_lvl()

        return ComparisonFunction.create_final_dict(score, lvl)

    def _get_writable_results(self):
        results = super(BranchCombiner, self)._get_writable_results()
        for child in self.get_children():
            results[child.get_name()] = child._get_writable_results()
        return results

    def evaluate(self):
        """
        Evaluate this BranchCombiner by evluating all its children and
        combining the results. If the results have already been calculated,
        this method returns silently.
        """
        if self._results:
            return

        for child in self.get_children():
            child.evaluate()

        self._results = self.__calc_results()

    def is_relevant(self):
        return bool(self.get_children())

class RootCombiner(BranchCombiner):
    MASTER_COMBINER_NAME = "MasterCombiner"    # name of the root combiner

    def __init__(self, desc_dict, eval_dict, data_file_path, ref_file_path, runnr):
        """
        Parameters:
            desc_dict     : a combiner description dictionary (which describes the tree structure)
            eval_dict     : an evaluation dictionary that holds information about analysis method for each histogram
            data_file_path: a path to a root file with data to analyse
            ref_file_path : a path to a root file that serves as reference
        """
        super(RootCombiner, self).__init__(RootCombiner.MASTER_COMBINER_NAME, desc_dict, eval_dict, data_file_path, ref_file_path)
        self.runnr = runnr

    def write_to_db(self):
        """
        Write the entire tree to the DQ database.
        """
        fileName = Config().dq_db_file_path

        writer = DQDB(fileName, False)
        writer.fill(self.runnr, self._get_writable_results())
        writer.close()

    def is_relevant(self):
        return True # Root is always relevant

    def _get_writable_results(self):
        results = super(RootCombiner, self)._get_writable_results()
        results["endtimestamp"] = RunDB().endtime(self.runnr)
        return results

class LeafCombiner(Combiner):
    COMP_KEY = "comparison"
    PARAMS_KEY  = "params"

    def __init__(self, name, desc_dict, eval_dict, data_file_path, ref_file_path):
        super(LeafCombiner, self).__init__(name, desc_dict, data_file_path, ref_file_path)

        if not self.get_name() in eval_dict:
            raise ComparisonFunctionNotFoundInEvalDictException(self.get_name())
        my_eval = eval_dict[self.get_name()]

        self.__max_warning = desc_dict[self.get_name()]["maxWarning"]
        self.__max_error = desc_dict[self.get_name()]["maxError"]

        self.__set_compare_func(my_eval)
        self.__trending = my_eval["trending"]
        self.__path = self._desc_dict["path"]

    def __str__(self):
        """Get a string representation of this object."""
        log_string = self.string_rep()
        log_string.append("Combiner's histogram: {}".format(self._desc_dict["path"]))
        return "\n".join(log_string)

    def get_path(self):
        """
        Get the path of this LeafCombiner, that is, the path in the data and
        reference ROOT files where the relevant histogram is located.
        """
        return self.__path

    def __set_compare_func(self, my_eval):
        """
        Read the compare function and its arguments from the eval dictionary.
        """
        self.compare_class = my_eval[LeafCombiner.COMP_KEY]
        function = self.compare_class.compare
        if not callable(function):
            raise ComparisonFunctionNotCallableException(self.get_name(), function.__name__)
        self.__compare_func = function

        arg = ""
        if LeafCombiner.PARAMS_KEY in my_eval:
            arg = my_eval[LeafCombiner.PARAMS_KEY]
        self.compare_arg = arg if arg else ""

    def __get_histos(self, data_file, ref_file):
        """Get histograms from files."""
        hist_name = self.get_path()
        data_hist = None
        ref_hist = None

        if data_file:
            data_hist = data_file.Get(hist_name)
        if not data_hist:
            raise HistogramNotFoundException(self.get_name(), hist_name)

        if ref_file:
            ref_hist = ref_file.Get(hist_name)
        if not ref_hist:
            raise HistogramNotFoundException(self.get_name(), hist_name)

        return data_hist, ref_hist

    def __calc_level(self, score):
        if score < self.__max_error:
            return ERROR_LEVELS.ERROR
        if score < self.__max_warning:
            return ERROR_LEVELS.WARNING
        return ERROR_LEVELS.OK

    def __evaluate_trends(self, data_hist):
        for trend in self.__trending:
            if isinstance(trend, tuple):
                trend_result = trend[0].calculate(data_hist, trend[1:])
            else:
                trend_result = trend.calculate(data_hist, None)
            self._results.update(trend_result)

    def evaluate(self):
        """
        Evaluate this leaf by performing a check on the data histogram, either
        directly or by comparing it to the corresponding reference histogram.
        """
        if self._results:
            return

        data_file = ROOT.TFile.Open(self._data_file_path, "READ")
        ref_file = ROOT.TFile.Open(self._ref_file_path, "READ")
        data_file, ref_file = detect_zombies(data_file, ref_file)

        try:
            data_hist, ref_hist = self.__get_histos(data_file, ref_file)
            score = self.__compare_func(data_hist, ref_hist, self.compare_arg)
            level = self.__calc_level(float(score))
            self._results = {"lvl": level, "score": score}
            self.__evaluate_trends(data_hist)
        except HistogramNotFoundException:
            self._results = {"lvl": ERROR_LEVELS.OK, "score": Score(Score.MAX)}

        data_file.Close()
        ref_file.Close()

        self._error_lvls[self.get_level()] = 1

    def is_relevant(self):
        return True # Leaf nodes are always relevant

