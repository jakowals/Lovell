"""Methods dealing with DQ values."""
import itertools

import veloview.analysis
from veloview.config import Config
from veloview.core.io import DQDB

def get_comment(runnr):
    db = DQDB(Config().dq_db_file_path)
    value = db.get_comment(runnr)
    db.close()
    return value

def set_comment(runnr, comment):
    db = DQDB(Config().dq_db_file_path)
    db.set_comment(runnr, comment)
    db.close()

def get_trending_variables(expert = False):
    dqVarsParser = DqVarsParser(True, True, expert)
    return dqVarsParser.get_dq_variables()

def get_dq_vars():
    return DqVarsParser().get_dq_variables()

def get_dq_values(runnr):
    db = DQDB(Config().dq_db_file_path)
    values = db.read(runnr)
    db.close()
    return values

def get_dq_tree(runnr):
    """Return DQ values as nested dictionaries, one per DQ level."""
    d = dict()
    for key, val in get_dq_values(runnr).iteritems():
        components = key.split('.')
        subd = d
        # Go down to the lowest-but-one level
        for comp in components[:-1]:
            if comp not in subd:
                subd[comp] = dict()
            subd = subd[comp]
        # The lowest level is the label of the value
        subd[components[-1]] = val
    return d

def get_sensor_numbers():
    return itertools.chain(range(0, 42), range(64, 106))

class DqVarsParser(object):
    def __init__(self, trendingOnly = False, includeVars = False, expertMode = False):
        self.__trendingOnly = trendingOnly
        self.__includeVars = includeVars
        self.__expertMode = expertMode

    def get_dq_variables(self):
        result = self.__add_variables_branch(Config().analysis_config, '', Config().analysis_config[0]['MasterCombiner'])
        db = DQDB(Config().dq_db_file_path)
        vars_in_db = db.vars()
        db.close()
        failed_vars = []
        for var in result:
            if not var[0] in vars_in_db:
                failed_vars.append(var)
        for failed_var in failed_vars:
            print("Var {} in result but not in db!".format(failed_var[0]))
            result.remove(failed_var)
        return result

    def __add_variables_branch(self, config, key, branch, base = ''):
        result = []

        for child in branch['children']:
            if child in config[0].keys():
                result += self.__add_variables_branch(config, child, config[0][child], '{}.{}'.format(base, child) if base else child)
            elif child in config[1].keys():
                result += self.__add_variables_leaf(('{}.{}'.format(base, child)) if base else child, config[1][child])

        if result and self.__includeVars:
            result.append(('{}.score'.format(base) if base else 'score', branch['title'] + ' DQ score'))

        return result

    def __add_variables_leaf(self, key, leaf):
        if self.__trendingOnly and not leaf.get('trending', False):
            return []

        sensor_dep = leaf.get("sensor_dependent", False)

        result = []
        if self.__expertMode and sensor_dep:
            for sensor_id in get_sensor_numbers():
                if self.__includeVars:
                    functionClass = getattr(veloview.analysis, leaf["comparison"])
                    variables = functionClass.vars()
                    result += [(key + '.' + key.split('.')[-1] + "_{0:03d}".format(sensor_id) + '.' + var, leaf['title'] + ' Sensor {0:03d}'.format(sensor_id)) for var in variables]
                else:
                    result.append((key + '.' + key.split('.')[-1] + "_{0:03d}".format(sensor_id), leaf['title'] + ' Sensor {0:03d}'.format(sensor_id)))

        if self.__includeVars and not sensor_dep:
            variables = []
            for trend in leaf.get("trending", []):
                trendClassName = trend[0] if isinstance(trend, tuple) else trend
                trendClass = getattr(veloview.analysis, trendClassName)
                variables += trendClass.vars()
            return result + [(key + '.' + var, leaf['title'] + ' ' + var) for var in variables]

        result.append(('{}.score'.format(key), leaf['title'] + ' DQ score'))
        return result

