# -*- coding: utf-8 -*-

from functools import partial
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from veloview.tell1.calina_plots import kdeplot_op
from PyQt4.QtGui import (
    QGridLayout,
    QHBoxLayout,
    QPalette,
    QPushButton,
    QMessageBox,
    QWidget
)
from ROOT import TGraph
import matplotlib
import numpy
import logging

from plottables import ( 
    PlottableBase, 
    PlottableCalibration, 
    lPlottable, 
    PlottableCalina, 
    PlottableIV, 
    PlottableTV, 
    DEFAULT_DRAW_OPTIONS
)

logging.basicConfig(format='%(name)s:[%(levelname)s]: %(message)s')
logger = logging.getLogger('lovellGUI')
logger.setLevel(logging.DEBUG)

class NavigationToolbarMod(NavigationToolbar):
    toolitems = [t for t in NavigationToolbar.toolitems if
                 t[0] in ('Home', 'Pan', 'Zoom', 'Save')]


class PlotWidgetBase(QWidget):
    PlottableClass = lPlottable

    def __init__(self, params, parent=None):
        QWidget.__init__(self, parent)
        self.ext = ""
        self.params = params
        self.options = self.params.get('options', {})
        self.create_main_frame()
        self.plottables = []
        self.setup_plottables()
        self.xLims = []
        self.yLims = []
        self.zLims = []
        self._initialise_default_options()

    def on_draw(self, tab_ops_state, notify_box):
        pass

    def _initialise_default_options(self):
        for key, default_value in DEFAULT_DRAW_OPTIONS.iteritems():
            if key not in self.options:
                self.options[key] = default_value

    def setup_multiple_plottables(self):
        for param in self.params['plottables']:
            p = self.PlottableClass(self.axes, self.fig, param, self)
            self.plottables.append(p)

    def setup_single_plottable(self):
        p = self.PlottableClass(self.axes, self.fig, self.params, self)
        self.plottables.append(p)

    def setup_plottables(self):
        if 'plottables' in self.params:
            self.setup_multiple_plottables()
        elif 'name' in self.params:
            self.setup_single_plottable()

    def create_main_frame(self):
        matplotlib.rcParams.update({'axes.titlesize': 'medium'})
        matplotlib.rcParams.update({'legend.fontsize': 'medium'})
        matplotlib.rcParams.update({'legend.framealpha': '0.5'})
        fig_color = self.palette().color(QPalette.Background)
        self.fig = Figure(facecolor=[fig_color.red() / 255., fig_color.blue() / 255., fig_color.green() / 255.])
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.axes = self.fig.add_subplot(111)
        self.mpl_toolbar = NavigationToolbarMod(self.canvas, self)
        grid_layout = QGridLayout()
        grid_layout.addWidget(self.canvas, 0, 0)
        menu = QWidget(self)
        hbox_layout = QHBoxLayout()
        menu.setLayout(hbox_layout)
        hbox_layout.addWidget(self.mpl_toolbar)
        grid_layout.addWidget(menu, 1, 0)
        hbox_layout.setContentsMargins(0, 0, 0, 0)
        grid_layout.setContentsMargins(0, 0, 0, 0)
        grid_layout.setSpacing(0)
        self.setLayout(grid_layout)
        grid_layout.setRowStretch(0, 10)
        self.tipBut = QPushButton("?")
        self.tipBut.clicked.connect(self.show_tip)
        hbox_layout.addWidget(self.tipBut)

    def show_tip(self):
        if 'tip' in self.params:
            tip = self.params['tip']
        else:
            tip = 'No tip supplied.'
        box = QMessageBox()
        box.setText(tip)
        box.exec_()

class SimplePlotWidget(PlotWidgetBase):

    def on_draw(self, tab_ops_state, notify_box):
        for p in self.plottables:
            p.on_draw(tab_ops_state, notify_box)

class PlotWidgetCalibration(SimplePlotWidget):
    PlottableClass = PlottableCalibration

    def on_draw(self, tab_ops_state, notify_box):
        self.axes.set_title(self.params['title'])
        del self.xLims[:]
        del self.yLims[:]
        del self.zLims[:]

        logger.debug('(Re)Plotting {}'.format(self.params['title']))
        self.axes.clear()
        SimplePlotWidget.on_draw(self, tab_ops_state, notify_box)
        self.axes.grid()
        self.axes.set_title(self.params['title'] + self.ext)
        if len(self.xLims) == 2:
            self.axes.set_xlim(self.xLims)
        if len(self.yLims) == 2:
            self.axes.set_ylim(self.yLims)
        self.fig.tight_layout()
        if self.options['legend']:
            self.axes.legend(loc='best')
        self.canvas.draw()
        self.fig.tight_layout()


class PlotWidgetCalina(SimplePlotWidget):
    PlottableClass = PlottableCalina

    def setup_multiple_plottables(self):
        raise NotImplementedError('Calina plot does not support multiple plottables')

    def on_draw(self, tab_ops_state, notify_box):
        self.axes.set_title(self.params['title'])
        del self.xLims[:]
        del self.yLims[:]
        del self.zLims[:]

        logger.debug('(Re)Plotting {}'.format(self.params['title']))
        self.axes.clear()

        SimplePlotWidget.on_draw(self, tab_ops_state, notify_box)

        self.axes.grid()
        #self.axes.set_title(self.params['title'] + self.ext)
        if self.options['legend']:
            self.axes.legend(loc='best')
        self.canvas.draw()
        self.fig.tight_layout()



class mplWidget(SimplePlotWidget):
    def __init__(self, params, parent=None):
        PlotWidgetBase.__init__(self, params, parent)

    def on_draw(self, tab_ops_state, notify_box):
        self.axes.set_title(self.params['title'])
        del self.xLims[:]
        del self.yLims[:]
        del self.zLims[:]

        logger.debug('(Re)Plotting {}'.format(self.params['title']))
        self.axes.clear()
        for p in self.plottables:
            p.on_draw(tab_ops_state, notify_box)
        self.axes.grid()
        self.axes.set_title(self.params['title'] + self.ext)

        if 'xrange' in self.params:
            self.axes.set_xlim(self.params['xrange'])
        else:
            if len(self.xLims) == 2:
                self.axes.set_xlim(self.xLims)
        manual_set_y_range = False
        if tab_ops_state.refDiff == False and tab_ops_state.refRatio == False:
            manual_set_y_range = True
        if tab_ops_state.displayRefs == False:
            manual_set_y_range = True

        if manual_set_y_range:
            if 'yrange' in self.params:
                logger.debug('Setting y axis range as per run_view configuration')
                self.axes.set_ylim(self.params['yrange'])
            else:
                logger.debug( 'Setting y axis range automatically')
                if len(self.yLims) == 2:
                    logger.debug("Limits ({}, {})".format( self.yLims[1], self.yLims[0] ))
                    shift = 0.05 * (self.yLims[1] - self.yLims[0])
                    self.axes.set_ylim([self.yLims[0] - shift, self.yLims[1] + shift])
            if 'zrange' in self.params:
                self.plottables[0].cax.set_clim(vmin=self.params['zrange'][0], vmax=self.params['zrange'][1])
        else:
            logger.debug( 'Automatically setting y axis range (using matplotlib).')

        self.fig.tight_layout()
        if self.options['legend']:
            if manual_set_y_range:
                self.axes.legend(loc='best')
        self.canvas.draw()
        self.fig.tight_layout()

    def on_draw_trend(self, tab_ops_state):
        self.axes.set_title(self.params['title'])
        del self.xLims[:]
        del self.yLims[:]
        del self.zLims[:]

        logger.debug('(Re)Plotting {}'.format(self.params['title']))
        self.axes.clear()
        for p in self.plottables:
            p.on_draw_trend(tab_ops_state)
        self.axes.grid()
        self.axes.set_title(self.params['title'] + self.ext)

        if 'xrange' in self.params:
            self.axes.set_xlim(self.params['xrange'])
        else:
            if len(self.xLims) == 2:
                self.axes.set_xlim(self.xLims)

        self.fig.tight_layout()
        self.canvas.draw()
        self.fig.tight_layout()

class PlotWidgetIV(mplWidget) : 
    PlottableClass = PlottableIV

class PlotWidgetTV(mplWidget) : 
    PlottableClass = PlottableTV
