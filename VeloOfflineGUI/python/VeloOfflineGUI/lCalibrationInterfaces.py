from veloview.runview import plots, utils
from veloview.config import Config
import calibration_view
import os

def calibrationview_config():
    return calibration_view.special_view_pages

def run_list(run_data_dir):
    run_list_path = os.path.join(run_data_dir, 'RunList.txt')
    with open(run_list_path, "r") as run_list_file:
        raw_run_list = run_list_file.read().splitlines()
    sorted_run_list = sorted(raw_run_list, reverse=True)
    return sorted_run_list

def run_name_to_path(run_name):
    run_name = str(run_name)
    run_name = run_name.split()[0]
    year, month, day = run_name.split('-')[0].split('_')
    dir_path_from_run = "/".join([year, month, day])
    root_file_name = run_name+".root"
    root_file_path_from_run_name = os.path.join(dir_path_from_run,root_file_name)
    return root_file_path_from_run_name

def add_sensor_number_to_plot_path(plot_path, sensor_number):
    sensor_name = "VeloTELL1Board{}".format(sensor_number)
    total_plot_path = "/".join([plot_path,sensor_name])
    return total_plot_path

def calibration_plot(run_name, plot_name, sensor_number, run_data_dir, ref_run ='Auto', get_ref=False, normalise=False, notify_box = None):
    Config().run_data_dir = run_data_dir
    relative_run_root_path = run_name_to_path(run_name)
    plot_path = add_sensor_number_to_plot_path(plot_name, sensor_number)
    return plots.get_special_plot(plot_path, relative_run_root_path, normalise=normalise, notify_box=notify_box)

def calina_plot(database_path, plot_type, day=None, sensor_number=None):
    data = plots.get_formatted_calina_data(database_path=database_path, plot_type=plot_type, day=day, sensor_number=sensor_number)
    return data

def calina_stats_plot(database_path, plot_type, module_type='R', task_type='calib', additional_condition=None, **kwargs):
    data = plots.get_formatted_calina_data(database_path=database_path, plot_type=plot_type, task_type=task_type, sensor_type=module_type, additional_condition=additional_condition, **kwargs)
    return data