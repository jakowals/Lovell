# -*- coding: utf-8 -*-
import lCalibrationInterfaces
import lInterfaces
import colormaps as new_maps
from veloview.config.run_view import DEFAULT_DRAW_OPTIONS
import numpy
import abc
from functools import partial
import logging

logging.basicConfig(format='%(name)s:[%(levelname)s]: %(message)s')
logger = logging.getLogger('lovellCalibrationWidget.VeloOfflineGui')
logger.setLevel(logging.DEBUG)

class PlottableBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, axes, fig, params, l_plot):
        self.axes = axes
        self.fig = fig
        self.color_bar_set = False
        self.params = params
        self.plot = l_plot
        self.options = self.params.get('options', {})
        self._initialise_default_options()

    def _initialise_default_options(self):
        for key, default_value in DEFAULT_DRAW_OPTIONS.iteritems():
            if key not in self.options:
                self.options[key] = default_value

    def tab(self):
        return self.plot.parent()

    @abc.abstractmethod
    def on_draw(self, tab_ops_state, notify_box):
        pass

    def runview_1d_runview_return(self, nominal, is_ref=False, reference_style=3, reference_color='r'):
        nominal_binning = nominal['data']['data']['binning']
        xs = [ 0.5 * (bind[0] + bind[1]) for bind in nominal_binning[:-1] ]
        style = self.options['style']

        color = 'b'
        if is_ref:
            style = reference_style
            color = reference_color
        else:
            if self.options['color'] is not None:
                if self.options['color'] == 'r' and style == 0:
                    msg = 'Red bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                elif self.options['color'] == 'g' and style == 0:
                    msg = 'Green bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                else:
                    color = self.options['color']

        ys = nominal['data']['data']['values'][:-1]

        self.add_1d_plot(xs, ys, style, color)
        self.set_axes_lims(min_x=min(xs), max_x=max(xs),
                           min_y=min(ys), max_y=max(ys))

    def runview_1d_data_minus_ref(self, nominal, reference, infact_ratio=False):
        nominal_binning = nominal['data']['data']['binning']
        nominal_values = nominal['data']['data']['values']
        reference_velues = reference['data']['data']['values']
        xs = [ 0.5 * (bind[0] + bind[1]) for bind in nominal_binning ]

        ys = []
        for i in range(len(nominal_values)):
            if infact_ratio:
                if reference_velues[i] == 0.0:
                    ys.append(0)
                else:
                    ys.append(nominal_values[i] / (1. * reference_velues[i]))
            else:
                ys.append(nominal_values[i] - reference_velues[i])

        style = 0
        if not infact_ratio:
            color = 'r'
        else:
            color = 'g'
        self.add_1d_plot(xs, ys, style, color)
        min_x = min(xs)
        max_x = max(xs)
        if len(self.plot.xLims) == 0:
            self.plot.xLims.append(min_x)
            self.plot.xLims.append(max_x)
        elif min_x < self.plot.xLims[0]:
            self.plot.xLims[0] = min_x
        elif max_x > self.plot.xLims[1]:
            self.plot.xLims[1] = max_x

    def set_axis_titles(self, nominal):
        xlabel = 'Default x label'
        ylabel = 'Default y label'
        zlabel = 'Default z label'
        axes_labels = nominal['data']['data']['axis_titles']

        if 'axis_titles' in self.plot.params:
            axis_titles = self.plot.params['axis_titles']
            xlabel = axis_titles[0]
            ylabel = axis_titles[1]
            if len(axis_titles) == 3:
                zlabel = ['axis_titles'][2]

        elif 'axis_titles' in nominal:
            axis_titles = nominal['axis_titles']
            xlabel = axis_titles[0]
            ylabel = axis_titles[1]
            if len(axis_titles[1]) == 3:
                zlabel = ['axis_titles'][2]

        elif 'axis_titles' in nominal['data']['data']:
            xlabel = axes_labels[0]
            ylabel = axes_labels[1]

        return xlabel, ylabel, zlabel

    def process_y_lims(self, nominal, reference=None, reference_type='overlay'):
        y_mean_dict = self.params['y_mean_limits']
        if y_mean_dict and y_mean_dict != 'auto':
            mean_y = {
                'overlay': 512,
                'data_minus_ref': 0,
                'data_div_ref': 0,
            }[reference_type]
            y_mean_limits = y_mean_dict[reference_type]
            self.plot.yLims = map(lambda x: mean_y + x, y_mean_limits)

    def set_axes_lims(self, min_x, max_x, min_y, max_y):

        x_lims = self.plot.xLims
        y_lims = self.plot.yLims

        if len(x_lims) == 0:
            x_lims.append(min_x)
            x_lims.append(max_x)
        elif min_x < x_lims[0]:
            x_lims[0] = min_x
        elif max_x > x_lims[1]:
            x_lims[1] = max_x

        if len(y_lims) == 0:
            self.plot.yLims.append(min_y)
            self.plot.yLims.append(max_y)
        elif min_y < y_lims[0]:
            y_lims[0] = min_y
        elif max_y > y_lims[1]:
            y_lims[1] = max_y

    def add_1d_plot(self, xs, ys, style, color='b'):
        lab = 'Default'
        if 'title' in self.params:
            lab = self.params['title']
        if style == 0:
            xs_mod = []
            ys_mod = []
            half_bin_width = 0.5 * (xs[1] - xs[0])
            for i in range(len(xs)):
                xs_mod.append(xs[i] - half_bin_width)
                xs_mod.append(xs[i] + half_bin_width)
                ys_mod.append(ys[i])
                ys_mod.append(ys[i])

            self.axes.bar(xs[1], ys[1], width=0, align='center',
                          linewidth=0, alpha=0.5, color=color, label=lab)
            self.axes.fill_between(xs_mod, ys_mod, numpy.zeros(len(xs_mod)),
                                   alpha=0.5, color=color, edgecolor=None)
            self.axes.step(xs, ys, where='mid', linewidth=1.0, c='k')


        elif style == 1:
            self.axes.step(xs, ys, where='mid', linewidth=1.5, c=color, label=lab)

        elif style == 2:
            m = self.options['marker']
            self.axes.plot(xs, ys, '-o', marker=m, markerfacecolor=color, markeredgecolor='none', markersize=8, label=lab)

        elif style == 3:
            m = self.options['marker']
            self.axes.plot(xs, ys, 'o', marker=m, markerfacecolor=color, markeredgecolor='none', markersize=8, label=lab)

        elif style == 4:
            self.axes.plot(xs, ys, '-', color=color, linewidth=2.5, label=lab)

    def select_color_for_plot(self, is_ref=False) : 
        color = 'b'
        if is_ref:
            color = 'r'
        else:
            if self.options['color'] is not None:
                if self.options['color'] == 'r' and style == 0:
                    msg = 'Red bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                elif self.options['color'] == 'g' and style == 0:
                    msg = 'Green bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                else:
                    color = self.options['color']
        return color

    def select_1d_drawing_mode(self, tab_ops_state, nominal, reference) : 

        if 'binning' in nominal['data']['data'] and len(nominal['data']['data']['binning']) > 0:
            if not tab_ops_state.displayRefs:
                self.runview_1d_runview_return(nominal)
            elif tab_ops_state.overLayRef:
                self.runview_1d_runview_return(nominal)
                if reference:
                    self.runview_1d_runview_return(reference, True)
            elif tab_ops_state.refDiff:
                self.runview_1d_data_minus_ref(nominal, reference)
            elif tab_ops_state.refRatio:
                self.runview_1d_data_minus_ref(nominal, reference, True)

class PlottableCalibration(PlottableBase):

    def on_draw(self, tab_ops_state, notify_box):
        run_name = tab_ops_state.runNum
        reference_run_name = tab_ops_state.refRunNum
        sensor_number = tab_ops_state.module_id
        nominal = lCalibrationInterfaces.calibration_plot(run_name=run_name,
                                                          plot_name=self.params['name'],
                                                          run_data_dir=tab_ops_state.run_data_dir,
                                                          sensor_number=sensor_number)
        reference = None
        if reference_run_name != 'Auto':
            reference = lCalibrationInterfaces.calibration_plot(run_name=reference_run_name,
                                                                plot_name=self.params['name'],
                                                                run_data_dir=tab_ops_state.run_data_dir,
                                                                sensor_number=sensor_number)

        set_y_lims = partial(self.process_y_lims, nominal=nominal, reference=reference)
        if not tab_ops_state.displayRefs:
            self.runview_1d_runview_return(nominal)
            set_y_lims()
        elif tab_ops_state.overLayRef:
            self.runview_1d_runview_return(nominal)
            if reference:
                self.runview_1d_runview_return(reference, True, **self.options['reference'])
            set_y_lims()
        elif tab_ops_state.refDiff:
            set_y_lims(reference_type='data_minus_ref')
            self.runview_1d_data_minus_ref(nominal, reference)
        elif tab_ops_state.refRatio:
            set_y_lims(reference_type='data_div_ref')
            self.runview_1d_data_minus_ref(nominal, reference, True)

        xlabel, ylabel, zlabel = self.set_axis_titles(nominal=nominal)

        self.plot.axes.set_xlabel(xlabel)
        self.plot.axes.set_ylabel(ylabel)
        if self.color_bar_set:
            self.cbar.set_label(zlabel)

class PlottableCalina(PlottableBase):

    def on_draw(self, tab_ops_state, notify_box):
        additional_condition = self.check_options_for_range(tab_ops_state)
        try:
            if 'sensor' in self.params['title'].lower():
                self.on_draw_calina_stats_by_sensor(tab_ops_state, notify_box, additional_condition=additional_condition)
            else:
                self.on_draw_calina_stats_by_day(notify_box, additional_condition=additional_condition)
        except Warning as w:
            notify_box.notify(str(w))


    def check_options_for_range(self, tab_ops_state):
        additional_condition = None
        try:
            run_num = int(tab_ops_state.runNum)
            ref = int(tab_ops_state.refRunNum)
        except:
            return None
        else:
            if run_num!=ref:
                run_range = (run_num, ref)
                up = max(run_range)
                low = min(run_range)
                additional_condition = dict(upper_limit=up, lower_limit=low, type='runid_range')
            return additional_condition

    def request_calina_stats_data(self, notify_box, additional_condition=None, **kwargs):
        data = None
        try:
            data = lCalibrationInterfaces.calina_stats_plot(additional_condition=additional_condition, **kwargs)
        except ValueError:
            if additional_condition is not None:
                notify_box.notify('Warning! No elements in requested run range')
            else:
                raise
        else:
            if additional_condition is not None and additional_condition['type'] == 'runid_range':
                requested_size = additional_condition['upper_limit'] - additional_condition['lower_limit'] + 1
                actual_size = len(data)
                if actual_size != requested_size:
                    notify_box.notify(
                        'Warning! There are some missing elements, requested size:{} actual:{}'.format(requested_size,
                                                                                                       actual_size))
                ids = [d['runid'] for d in data]
                if additional_condition['upper_limit'] not in ids or additional_condition['lower_limit'] not in ids:
                    notify_box.notify(
                        'Warning! Some boundary runs are missing, requested:[{lower_limit}, {upper_limit}] , actual:[{min}, {max}]'.format(
                            min=min(ids), max=max(ids), **additional_condition))
        return data

    def on_draw_calina_stats_by_day(self, notify_box, additional_condition=None):
        task_type = 'noise' if 'noise' in self.params['title'].lower() else 'calib'
        limit = None
        if additional_condition is None:
            limit = self.params.get('query_limits', None)
        module_type = 'phi' if 'phi' in self.params['title'].lower() else 'R'
        kwargs = dict(
            module_type=module_type,
            task_type=task_type,
            limit=limit,
            additional_condition=additional_condition,
            database_path = self.params['calina_db_path'],
            plot_type = 'by_day'
        )
        nominal = self.request_calina_stats_data(notify_box=notify_box, **kwargs)
        self.calina_boxplots(nominal, title="Outlierness trend for {} {}-type sensors".format(task_type, module_type))

    def on_draw_calina_stats_by_sensor(self, tab_ops_state, notify_box, **kwargs):
        path = self.params['calina_db_path']
        sensor_number = tab_ops_state.module_id
        nominal = lCalibrationInterfaces.calina_stats_plot(path,'by_sensor', sensor_number=sensor_number)
        self.calina_boxplots(nominal, title="Outlierness trend for sensor: {}".format(sensor_number))


    def calina_kde_plot(self, data):
        plotdata = data['data']['data']
        plotdata = plotdata.reshape(plotdata.shape[0], 1)
        kdeplot_op(self.axes, plotdata, bw=4.5)

    def calina_boxplots(self, data, title):
        #@TODO
        self.axes.bxp(data)
        self.axes.set_title(title)
        import matplotlib.dates as mdates
        myFmt = mdates.DateFormatter('%Y-%m-%d')
        self.fig.autofmt_xdate()

class PlottableTV(PlottableBase):

    def on_draw(self, tab_ops_state, notify_box):

        if 'normalise' in self.params and self.params['normalise']:
            norm = True
        else:
            norm = False

        module_id = tab_ops_state.module_id + self.options['sensorShift']

        if tab_ops_state.displayRefs:
            data_iv_file = str(self.tab().dataIvFile())
            ref_iv_file = str(self.tab().refIvFile())
            self.plot.ext = ' - sensor {}'.format(module_id)
            nominal, reference = lInterfaces.TV_plot(data_iv_file,
                                                     ref_iv_file,
                                                     module_id,
                                                     tab_ops_state.iv_data_dir,
                                                     get_ref=True,
                                                     notify_box=notify_box)
            self.specialanalyses_1d_tv_return(nominal)
            self.specialanalyses_1d_tv_return(reference, is_ref=True)

        else:
            data_iv_file = str(self.tab().dataIvFile())
            ref_iv_file = str(self.tab().refIvFile())
            self.plot.ext = ' - sensor {}'.format(module_id)
            nominal = lInterfaces.TV_plot(data_iv_file,
                                          ref_iv_file,
                                          module_id,
                                          tab_ops_state.iv_data_dir,
                                          notify_box=notify_box)
            self.specialanalyses_1d_tv_return(nominal)

        self.select_1d_drawing_mode(tab_ops_state, nominal, reference)

        xlabel, ylabel, zlabel = self.set_axis_titles(nominal)
        self.plot.axes.set_xlabel(xlabel)
        self.plot.axes.set_ylabel(ylabel)
        if self.color_bar_set:
            self.cbar.set_label(zlabel)

    def specialanalyses_1d_tv_return(self, nominal, is_ref=False):
        xs = nominal['data']['data']['x']
        ys = nominal['data']['data']['y']
        style = self.options['style']

        color = self.select_color_for_plot(is_ref)

        self.add_1d_plot_tv(xs, ys, color, is_ref)
        self.set_axes_lims(min_x=min(xs), max_x=max(xs),
                           min_y=min(ys), max_y=max(ys))

    def add_1d_plot_tv(self, xs, ys, color='b', is_ref=False):
        if not is_ref:
            lab = 'Nominal'
            m = self.options['marker']
            self.axes.plot(xs, ys, '-o', marker=m, color=color,
                           markerfacecolor=color, markeredgecolor='none',
                           markersize=8, label=lab)

        elif is_ref:
            lab = 'Reference'
            m = self.options['marker']
            self.axes.plot(xs, ys, '-o', marker=m, color=color,
                           markerfacecolor=color, markeredgecolor='none',
                           markersize=8, label=lab)



class PlottableIV(PlottableBase):

    def on_draw(self, tab_ops_state, notify_box):
        if 'normalise' in self.params and self.params['normalise']:
            norm = True
        else:
            norm = False
        module_id = tab_ops_state.module_id + self.options['sensorShift']
        if tab_ops_state.displayRefs:
            data_iv_file = str(self.tab().dataIvFile())
            ref_iv_file = str(self.tab().refIvFile())
            self.plot.ext = ' - sensor {}'.format(module_id)
            is_temp_corr = self.tab().is_temp_corrected()
            if is_temp_corr:
                nominal, reference = lInterfaces.IV_plot_corr(data_iv_file,
                                                              ref_iv_file,
                                                              module_id,
                                                              tab_ops_state.iv_data_dir,
                                                              get_ref=True,
                                                              notify_box=notify_box)
                self.specialanalyses_1d_iv_return(nominal, temp_corr=True)
                self.specialanalyses_1d_iv_return(reference, temp_corr=True, is_ref=True)
            else:
                nominal, reference, nominal_temp, reference_temp = lInterfaces.IV_plot(data_iv_file,
                                                                                       ref_iv_file,
                                                                                       module_id,
                                                                                       tab_ops_state.iv_data_dir,
                                                                                       get_ref=True,
                                                                                       notify_box=notify_box)
                self.specialanalyses_1d_iv_return(nominal, nominal_temp)
                self.specialanalyses_1d_iv_return(reference, reference_temp, is_ref=True)
        else:

            data_iv_file = str(self.tab().dataIvFile())
            ref_iv_file = str(self.tab().refIvFile())
            self.plot.ext = ' - sensor {}'.format(module_id)
            is_temp_corr = self.tab().is_temp_corrected()
            if is_temp_corr:
                nominal = lInterfaces.IV_plot_corr(data_iv_file,
                                                   ref_iv_file,
                                                   module_id,
                                                   tab_ops_state.iv_data_dir,
                                                   notify_box=notify_box)
                self.specialanalyses_1d_iv_return(nominal, temp_corr=True)
            else:
                nominal, reference_temp = lInterfaces.IV_plot(data_iv_file,
                                                              ref_iv_file,
                                                              module_id,
                                                              tab_ops_state.iv_data_dir,
                                                              notify_box=notify_box)
                self.specialanalyses_1d_iv_return(nominal, reference_temp)

            reference = None

        if nominal == None : return

        self.select_1d_drawing_mode(tab_ops_state, nominal, reference)

        xlabel, ylabel, zlabel = self.set_axis_titles(nominal)
        self.plot.axes.set_xlabel(xlabel)
        self.plot.axes.set_ylabel(ylabel)
        if self.color_bar_set:
            self.cbar.set_label(zlabel)

    def specialanalyses_1d_iv_return(self, nominal, temp=None, temp_corr=False, is_ref=False):
        xs = nominal['data']['data']['x']
        ys = nominal['data']['data']['y']
        style = self.options['style']

        color = self.select_color_for_plot(is_ref)

        self.add_1d_plot_iv(xs, ys, temp, color, is_ref)
        self.set_axes_lims(min_x=min(xs), max_x=max(xs),
                           min_y=min(ys), max_y=max(ys))

    def add_1d_plot_iv(self, xs, ys, temp, color='b', is_ref=False):
        if not is_ref:
            if temp == None:
                lab = 'Nominal'
            else:
                lab = 'Nominal: {0:.1f} °C'.format(temp)
            m = self.options['marker']
            self.axes.plot(xs, ys, '-o', marker=m, color=color,
                           markerfacecolor=color, markeredgecolor='none',
                           markersize=8, label=lab)

        elif is_ref:
            if temp == None:
                lab = 'Reference'
            else:
                lab = 'Reference: {0:.1f} °C'.format(temp)
            m = self.options['marker']
            self.axes.plot(xs, ys, '-o', marker=m, color=color,
                           markerfacecolor=color, markeredgecolor='none',
                           markersize=8, label=lab)

class lPlottable(PlottableBase):

    def on_draw_trend(self, tab_ops_state):
        initial_run = int(self.tab().trendStartNum())
        final_run = int(self.tab().trendEndNum())
        trend_variable = str(self.tab().trendVariable())
        two_dimension = bool(self.tab().is2d())
        if initial_run < final_run:
            nominal = lInterfaces.veloview_plot(trend_variable,
                                                (initial_run, final_run),
                                                tab_ops_state.run_data_dir)
            self.veloview_1d_trending_return(nominal)
            if two_dimension:
                trend_variable2d = str(self.tab().trendVariable2d())
                nominal = lInterfaces.veloview_plot2d(trend_variable,
                                                      trend_variable2d,
                                                      (initial_run, final_run),
                                                      tab_ops_state.run_data_dir)
                self.veloview_2d_trending_return(nominal)

                axes_labels = nominal['data']['data']['axis_titles']

                xlabel = axes_labels[0]
                ylabel = axes_labels[1]

                self.plot.axes.set_xlabel(xlabel)
                self.plot.axes.set_ylabel(ylabel)
        else:
            msg = 'Please choose valid range - final run greater then initial run'
            logger.info(msg)


    def on_draw(self, tab_ops_state, notify_box):
        if 'normalise' in self.params and self.params['normalise']:
            norm = True
        else:
            norm = False
        module_id = tab_ops_state.module_id + self.options['sensorShift']
        if tab_ops_state.displayRefs:
            if 'sensor_dependent' in self.plot.params and self.plot.params['sensor_dependent']:
                self.plot.ext = ' - sensor {}'.format(module_id)
            ref_run = tab_ops_state.refRunNum
            try:
                ref_run = int(ref_run)
            except ValueError:
                ref_run = None
            nominal, reference = lInterfaces.runview_plot(int(tab_ops_state.runNum), self.params['name'],
                                                          module_id, str(tab_ops_state.run_data_dir),
                                                          ref_run=ref_run, get_ref=True,
                                                          notify_box=notify_box,
                                                          normalise=norm)

        else:

            if 'sensor_dependent' in self.plot.params and self.plot.params['sensor_dependent']:
                self.plot.ext = ' - sensor {}'.format(module_id)
            nominal = lInterfaces.runview_plot(int(tab_ops_state.runNum), self.params['name'],
                                               module_id, tab_ops_state.run_data_dir,
                                               normalise=norm, notify_box=notify_box)
            reference = None

        if nominal == None : return

        if 'xbinning' in nominal['data']['data'] and len(nominal['data']['data']['xbinning']) > 0:
            if self.options['asText']:
                self.runview_2d_text(nominal)
            elif tab_ops_state.refDiff and tab_ops_state.displayRefs:
                self.runview_2d_data_minus_ref(nominal, reference)
            elif tab_ops_state.refRatio and tab_ops_state.displayRefs:
                self.runview_2d_data_minus_ref(nominal, reference, True)
            else:
                self.runview_2d(nominal)
        else : 
            self.select_1d_drawing_mode(tab_ops_state, nominal, reference)

        xlabel, ylabel, zlabel = self.set_axis_titles(nominal)
        self.plot.axes.set_xlabel(xlabel)
        self.plot.axes.set_ylabel(ylabel)
        if self.color_bar_set:
            self.cbar.set_label(zlabel)


    def veloview_1d_trending_return(self, nominal, is_ref=False):
        if len(nominal['data']['data']['values']) == 0:
            return

        xs = map(lambda x: x[0], nominal['data']['data']['values'])
        style = self.options['style']

        color = 'b'
        if is_ref:
            color = 'r'
        else:
            if self.options['color']:
                if self.options['color'] == 'r' and style == 0:
                    msg = 'Red bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                elif self.options['color'] == 'g' and style == 0:
                    msg = 'Green bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                else:
                    color = self.options['color']

        style = 2

        ys = map(lambda x: x[1], nominal['data']['data']['values'])
        self.add_1d_plot(xs, ys, style, color)
        self.set_axes_lims(min_x=min(xs), max_x=max(xs),
                           min_y=min(ys), max_y=max(ys))



    def runview_2d_text(self, nominal):
        x_binning = nominal['data']['data']['xbinning']
        nominal_values = nominal['data']['data']['values']
        half_bin_x = 0.5 * (x_binning[0][1] + x_binning[0][0])

        for i in range(len(nominal_values)):
            for j in range(len(nominal_values[0])):
                z = nominal_values[i][j]

                if z == 0.0:
                    continue
                x = x_binning[i][1] + half_bin_x + 0.6

                if int(z) % 2 == 0:
                    y = 2.5
                else:
                    y = -2.5
                self.axes.annotate(str(int(z)), xy=(x, y), xycoords='data', fontsize=9,
                                   horizontalalignment='center', verticalalignment='center', rotation=90,
                                   weight='demibold')


    def runview_2d_data_minus_ref(self, nominal, reference, infact_ratio=False):
        x_binning = nominal['data']['data']['xbinning']
        y_binning = nominal['data']['data']['ybinning']
        min_x = x_binning[0][0]
        max_x = x_binning[-1][1]
        min_y = y_binning[0][0]
        max_y = y_binning[-1][1]

        nominal_values = nominal['data']['data']['values']
        reference_velues = reference['data']['data']['values']
        py_bins = []
        for i in range(len(nominal_values)):
            bins = []
            for j in range(len(nominal_values[0])):
                if infact_ratio:
                    if reference['data']['data']['values'][i][j] == 0.0:
                        z = 0
                    else:
                        z = nominal_values[i][j] / (1. * reference_velues[i][j])
                else:
                    z = nominal_values[i][j] - reference_velues[i][j]
                bins.append(z)
            py_bins.append(bins)

        numpy_bins = numpy.empty([len(py_bins[0]), len(py_bins)])
        for i in range(len(py_bins)):
            for j in range(len(py_bins[0])):
                numpy_bins[len(py_bins[0]) - j - 1][i] = py_bins[i][j]

        numpy_bins[numpy_bins == 0.0] = numpy.nan

        self.cax = self.axes.imshow(numpy_bins,
                                    interpolation='none', cmap=new_maps.viridis,
                                    extent=[min_x, max_x, min_y, max_y],
                                    aspect='auto'
                                    )

        if not self.color_bar_set:
            self.cbar = self.fig.colorbar(self.cax)
            self.color_bar_set = True

        else:
            self.cbar.on_mappable_changed(self.cax)

        self.set_axes_lims(min_x, max_x, min_y, max_y)

    def runview_2d(self, nominal):
        x_binning = nominal['data']['data']['xbinning']
        y_binning = nominal['data']['data']['ybinning']
        min_x = x_binning[0][0]
        max_x = x_binning[-1][1]
        min_y = y_binning[0][0]
        max_y = y_binning[-1][1]

        py_bins = nominal['data']['data']['values']
        numpy_bins = numpy.empty([len(py_bins[0]), len(py_bins)])
        for i in range(len(py_bins)):
            for j in range(len(py_bins[0])):
                numpy_bins[len(py_bins[0]) - j - 1][i] = py_bins[i][j]

        numpy_bins[numpy_bins == 0.0] = numpy.nan

        self.cax = self.axes.imshow(numpy_bins,
                                    interpolation='none', cmap=new_maps.viridis,
                                    extent=[min_x, max_x, min_y, max_y],
                                    aspect='auto'
                                    )

        if not self.color_bar_set:
            self.cbar = self.fig.colorbar(self.cax, aspect=12)
            self.color_bar_set = True

        else:
            self.cbar.on_mappable_changed(self.cax)

        self.set_axes_lims(min_x, max_x, min_y, max_y)

    def veloview_2d_trending_return(self, nominal, is_ref=False):
        xs = map(lambda x: x[0], nominal['data']['data']['values'])
        style = self.options['style']

        color = 'b'
        if is_ref:
            color = 'r'
        else:
            if self.options['color'] is not None:
                if self.options['color'] == 'r' and style == 0:
                    msg = 'Red bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                elif self.options['color'] == 'g' and style == 0:
                    msg = 'Green bars are reserved for references - defaulting to blue'
                    self.tab().notify(msg)
                else:
                    color = self.options['color']

        style = 3

        ys = map(lambda x: x[1], nominal['data']['data']['values'])
        self.add_1d_plot(xs, ys, style, color)
        self.set_axes_lims(min_x=min(xs), max_x=max(xs),
                           min_y=min(ys), max_y=max(ys))


